<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'semantic_wp477');

/** MySQL database username */
define('DB_USER', 'semantic_wp477');

/** MySQL database password */
define('DB_PASSWORD', '56b!2TSPE@');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ybxl9mhojwl4srbevqpqoutm7d6h0hl1xvw1vu3isfq5ejna65jyc8jmmbmeu55y');
define('SECURE_AUTH_KEY',  'kzcidxdc74ru7oatpew14kswkzqkvp1xshc5lrx4kzydyhw43eznj9zq0qujbsb6');
define('LOGGED_IN_KEY',    'tceioyc7jwjbenedndvnotl3jvkwofhllme49kmssc9mz7iazqty33oszj5v8mdc');
define('NONCE_KEY',        'p1sozfbwdhh4kjxhyr69nipp8rnvecrc7chbuylorijhqub6k90ktsh4wtcmk8r1');
define('AUTH_SALT',        '77ojmnsodghlcrflwrxnh8iqchat1nl3y4ztd96npanxyopazln3sljytwrijjwj');
define('SECURE_AUTH_SALT', 'qtu8qnxargys6cbog072ueefo9t8maojp7k10u3danzsv4gjxqvfcmvihuwenfcn');
define('LOGGED_IN_SALT',   'zcmm7b4awn4eemwyyqg6jrqybwxmqmctq0kcoezhkr41gggxjok3dbshtxi0n3rd');
define('NONCE_SALT',       'jcxhobbw5yxmpktiwtz8nwhpcl5qpdjfwfj0w6l1wxvmmbybe1xf5ykrxxopoheu');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
